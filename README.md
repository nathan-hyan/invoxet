# Invoxet - Gaming at your fingertips.

Landing page dedicated to video-games and e-sports. built using **HTML**, **SCSS** and **JavaScript.**

## Installation

1. Just run index.html.

## Contact

- Email: [exequiel@hyan.dev](mailto:exequiel@hyan.dev)
- Website: https://hyan.dev
- LinkedIn: [https://www.linkedin.com/in/exequielm2048/](https://www.linkedin.com/in/exequielm2048/)
